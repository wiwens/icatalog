//
//  Company.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 19.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import SWXMLHash

struct Company: XMLIndexerDeserializable {
    
    let identifier: Int
    let industryId: Int
    let industriesId: [Int]
    let name: String
    let icon: String
    
}

extension Company {
    static func deserialize(xml: XMLIndexer) throws -> Company {
        var industriesIdentifires = [Int]()
        for id in xml["IndustriesID"]["int"] {
            industriesIdentifires.append(try id.value())
        }
        return try Company(
            identifier: xml["Id"].value(),
            industryId: xml["IndustryId"].value(),
            industriesId: industriesIdentifires,
            name: xml["Name"].value(),
            icon: xml["Icon"].value()
        )
    }
}
