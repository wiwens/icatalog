//
//  CustomTableViewCell.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 20.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit

class IndustryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var industryIcon: UIImageView!
    @IBOutlet weak var industryName: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        industryIcon.image = nil
        industryName.text = nil
    }
    
}
