//
//  UrlBuilder.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 21.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation

class UrlBuilder {
    
    func buildURLWithPath(path: String) -> NSURL {
        let urlComponents = NSURLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "recruitment-ws.future-processing.com"
        urlComponents.path = path
        return urlComponents.URL!
    }
    
}
