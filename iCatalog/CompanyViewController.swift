//
//  CatalogViewController.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 20.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash
import SwiftSpinner

class CompanyViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let companyService = CompanyService()
    private let parser = Parser()
    private var companiesArray = [Company]()
    private var allCompaniesArray = [Company]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.registerNib(UINib(nibName: "CompanyCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "CompanyCollectionViewCell")
        fetchCompanies()
        setupAppearance()
    }
    
    private func presentAlertControllerIfNeeded() {
        if (self.companiesArray.isEmpty) {
            let alertController = UIAlertController(title: "No resources", message: "This industry has no catalog.", preferredStyle: .Alert)
            let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(okAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    private func fetchCompanies() {
        SwiftSpinner.show("Fetching companies...")
        companyService.fetchCompaniesWithCompletion { [weak self] (companies, error) in
            SwiftSpinner.hide()
            if let error = error {
                print("Error \(error)")
            }
            if let companies = companies {
                self?.companiesArray = companies
                self?.allCompaniesArray = companies
                self?.collectionView.reloadData()
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.companiesArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CompanyCollectionViewCell", forIndexPath: indexPath) as! CompanyCollectionViewCell
        cell.companyIcon.image = parser.parseBase64ToUIImage(self.companiesArray[indexPath.row].icon)
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("showCatalog", sender: indexPath)
    }
    
    // MARK: Segue action
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showCatalog" {
            let destinationVC = segue.destinationViewController as? CatalogViewController
            guard let indexPath = sender as? NSIndexPath else { return }
            destinationVC?.chosenCompany(self.companiesArray[indexPath.row])
        } else if segue.identifier == "showIndustries" {
            let destinationVC = segue.destinationViewController as? IndustryViewController
            destinationVC?.delegate = self
        }
    }
    
    // MARK: Array operation
    
    private func filteredCompaniesForIndustry(industryIdentifier: Int) -> [Company] {
        return allCompaniesArray.filter { (company) -> Bool in
            company.industriesId.contains(industryIdentifier) ? true : false
        }
    }
    
    // MARK: Appearance
    
    func setupAppearance() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }

}

// MARK: IndustryViewControllerDelegate

extension CompanyViewController: IndustryViewControllerDelegate {
    func industryViewControllerDidTapOnAllIndustries(industryViewController: IndustryViewController) {
        industryViewController.navigationController?.popViewControllerAnimated(true)
        self.companiesArray = self.allCompaniesArray
        self.collectionView.reloadData()
        self.presentAlertControllerIfNeeded()
    }
    
    func industryViewControllerDidTapOnIndustryWithIdentifier(industryViewController: IndustryViewController, industryIdentifier: Int) {
        industryViewController.navigationController?.popViewControllerAnimated(true)
        self.companiesArray = self.filteredCompaniesForIndustry(industryIdentifier)
        self.collectionView.reloadData()
        self.presentAlertControllerIfNeeded()
    }
}
