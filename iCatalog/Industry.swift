//
//  Industry.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 20.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import SWXMLHash

struct Industry: XMLIndexerDeserializable {
    
    let identifier: Int
    let name: String
    let icon: String

}

extension Industry {
    static func deserialize(xml: XMLIndexer) throws -> Industry {
        return try Industry(
            identifier: xml["Id"].value(),
            name: xml["Name"].value(),
            icon: xml["Icon"].value()
        )
    }
}
