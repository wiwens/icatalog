//
//  MyCollectionViewCell.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 20.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit

class CompanyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var companyIcon: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        companyIcon.image = nil
    }
    
}
