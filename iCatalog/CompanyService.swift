//
//  CompanyService.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 21.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import Alamofire

protocol CompanyServiceProtocol {
    func fetchCompaniesWithCompletion(completion: (companies: [Company]?, error: NSError?) -> ())
}

class CompanyService: CompanyServiceProtocol {
    
    func fetchCompaniesWithCompletion(completion: (companies: [Company]?, error: NSError?) -> ()) {
        let path = "/Recruitment/iCatalog.asmx/GetAllCompanies"
        Alamofire.request(.GET, UrlBuilder().buildURLWithPath(path)).responseData { (response: Response<NSData, NSError>) in
            switch response.result {
            case .Success(_):
                let parser = Parser()
                guard let data = response.data else { return }
                let companies = parser.parseCompanies(data)
                completion(companies: companies, error: nil)
            case .Failure(let error):
                completion(companies: nil, error: error)
            }
        }
    }
    
}
