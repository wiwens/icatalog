//
//  CatalogService.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 21.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import Alamofire

protocol CatalogServiceProtocol {
    func fetchCatalogWithCompanyId(companyId: Int, completion: (catalog: NSData?, error: NSError?) -> ())
}

class CatalogService: CatalogServiceProtocol {
    
    func fetchCatalogWithCompanyId(companyId: Int, completion: (catalog: NSData?, error: NSError?) -> ()) {
        let path = "/Recruitment/iCatalog.asmx/GetCatalogByCompanyId"
        Alamofire.request(.GET, UrlBuilder().buildURLWithPath(path), parameters: ["companyId": String(companyId)]).responseData { (response: Response<NSData, NSError>) in
            switch response.result {
            case .Success(_):
                guard let data = response.data else { return }
                let catalog = Parser().parseCatalog(data)
                completion(catalog: catalog, error: nil)
            case .Failure(let error):
                completion(catalog: nil, error: error)
            }
        }
    }
    
}
