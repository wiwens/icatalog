//
//  IndustryService.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 21.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import Alamofire

protocol IndustryServiceProtocol {
    func fetchIndustriesWithCompletion(completion: (industries: [Industry]?, error: NSError?) -> ())
}

class IndustryService: IndustryServiceProtocol {
    
    func fetchIndustriesWithCompletion(completion: (industries: [Industry]?, error: NSError?) -> ()) {
        let path = "/Recruitment/iCatalog.asmx/GetAllIndustries"
        Alamofire.request(.GET, UrlBuilder().buildURLWithPath(path)).responseData { (response: Response<NSData, NSError>) in
            switch response.result {
            case .Success(_):
                guard let data = response.data else { return }
                let industries = Parser().parseIndustries(data)
                completion(industries: industries, error: nil)
            case .Failure(let error):
                completion(industries: nil, error: error)
            }
        }
    }
    
}
