//
//  Parser.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 20.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import SWXMLHash

class Parser {
    
    func parseCompanies(data: NSData) -> [Company] {
        var companiesArray = [Company]()
        let xml = SWXMLHash.parse(data)
        do {
            companiesArray = try xml["ArrayOfCompany"]["Company"].value()
        } catch {
            print("Error with mapping companies")
        }
        return companiesArray
    }
    
    func parseIndustries(data: NSData) -> [Industry] {
        var industriesArray = [Industry]()
        let xml = SWXMLHash.parse(data)
        do {
            industriesArray = try xml["ArrayOfIndustry"]["Industry"].value()
        } catch {
            print("Error with mapping industries")
        }
        return industriesArray
    }
    
    func parseCatalog(data: NSData) -> NSData? {
        let xml = SWXMLHash.parse(data)
        guard let base64String = xml["base64Binary"].element?.text else { return nil }
        return self.parseBase64StringToData(base64String)
    }
    
    func parseBase64ToUIImage(iconString: String) -> UIImage? {
        guard let decodedData = NSData(base64EncodedString: iconString, options: NSDataBase64DecodingOptions(rawValue: 0)) else { return nil }
        return UIImage(data: decodedData, scale: 1.0)
    }
    
    private func parseBase64StringToData(string: String) -> NSData? {
        return NSData(base64EncodedString: string, options: NSDataBase64DecodingOptions(rawValue: 0))
    }
    
}
