//
//  CompanyViewController.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 19.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash
import SwiftSpinner

protocol IndustryViewControllerDelegate: class {
    func industryViewControllerDidTapOnAllIndustries(industryViewController: IndustryViewController)
    func industryViewControllerDidTapOnIndustryWithIdentifier(industryViewController: IndustryViewController, industryIdentifier: Int)
}

class IndustryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    weak var delegate: IndustryViewControllerDelegate?

    @IBOutlet weak var tableView: UITableView!
    
    private let industryService: IndustryServiceProtocol = IndustryService()
    private var industriesArray = [Industry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNibs()
        fetchIndustries()
    }
    
    func fetchIndustries() {
        SwiftSpinner.show("Downloading industries...")
        industryService.fetchIndustriesWithCompletion { [weak self] (industries, error) in
            SwiftSpinner.hide()
            if let error = error {
                print("Error \(error)")
            }
            if let industries = industries {
                self?.industriesArray = industries
                self?.tableView.reloadData()
            }
        }
    }
    
    private func registerNibs() {
        self.tableView.registerNib(UINib(nibName: String(AllIndustriesTableViewCell), bundle: nil), forCellReuseIdentifier: String(AllIndustriesTableViewCell))
        self.tableView.registerNib(UINib(nibName: String(IndustryTableViewCell), bundle: nil), forCellReuseIdentifier: String(IndustryTableViewCell))
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.industriesArray.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return tableView.dequeueReusableCellWithIdentifier(String(AllIndustriesTableViewCell), forIndexPath: indexPath) as! AllIndustriesTableViewCell
        } else {
            let industryCell = tableView.dequeueReusableCellWithIdentifier(String(IndustryTableViewCell), forIndexPath: indexPath) as! IndustryTableViewCell
            industryCell.industryName.text = self.industriesArray[indexPath.row-1].name
            industryCell.industryIcon.image = Parser().parseBase64ToUIImage(self.industriesArray[indexPath.row-1].icon)
            return industryCell
        }
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) where cell is IndustryTableViewCell {
            delegate?.industryViewControllerDidTapOnIndustryWithIdentifier(self, industryIdentifier: industriesArray[indexPath.row-1].identifier)
        } else {
            delegate?.industryViewControllerDidTapOnAllIndustries(self)
        }
    }
    
}
