//
//  CatalogPDFViewController.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 20.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner

class CatalogViewController: UIViewController {
    
    private var company: Company!
    private let parser = Parser()
    private let catalogService: CatalogServiceProtocol = CatalogService()
    
    @IBOutlet weak var catalogWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchCatalogWithId(company.identifier)
        self.navigationItem.title = company.name
    }
    
    func chosenCompany(company: Company) {
        self.company = company
    }
    
    private func fetchCatalogWithId(identifier: Int) {
        SwiftSpinner.show("Downloading catalog...")
        catalogService.fetchCatalogWithCompanyId(company.identifier) { [weak self] (catalog, error) in
            SwiftSpinner.hide()
            if let error = error {
                print("Error \(error)")
            }
            guard let catalogData = catalog else { return }
            self?.loadDataToPDF(catalogData)
        }
    }
    
    func loadDataToPDF(data: NSData) {
        catalogWebView.loadData(data, MIMEType: "application/pdf", textEncodingName: "utf-8", baseURL: NSURL(fileURLWithPath: ""))
    }

}
