//
//  UIColor+ColorPallete.swift
//  iCatalog
//
//  Created by Wiktor Wielgus on 21.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func navigationBarColor() -> UIColor {
        return UIColor(red:0.11, green:0.15, blue:0.25, alpha:1.0)
    }
    
}
